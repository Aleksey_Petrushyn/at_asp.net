﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WorkForAutomation.Models
{
    public class LetterFactory
    {
        private const string EmailAddress = "epam.testing@yandex.com";
        public const string TitleForEmail = "MyTest";
        private const string TextOfLetter = "Hello! I'm Test";
        
        public static Letter Letter
        {
            get { return new Letter(EmailAddress, TitleForEmail, TextOfLetter); }
        }
    }
}
