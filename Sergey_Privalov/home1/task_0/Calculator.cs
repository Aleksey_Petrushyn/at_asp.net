﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_0
{
    public class Calculator : ICalculator
    {
        public double Division(int a, int b)
        {
            if (b == 0)
            {
                return 0;
            }
            return a / b;
        }

        public double Multiplication(int a, int b)
        {
            return a * b;
        }

        public double Subtraction(int a, int b)
        {
            return a - b;
        }

        public double Sum(int a, int b)
        {
            return a + b;
        }
    }
}
