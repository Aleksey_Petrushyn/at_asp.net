﻿namespace task_1.Models
{
    public class NewMail
    {
        public string SendTo { get; set; }
        public string Subject { get; set; }
        public string BodyMail { get; set; }

        public NewMail(string sendTo, string subject, string bodyMail)
        {
            SendTo = sendTo;
            Subject = subject;
            BodyMail = bodyMail;
        }
    }
}
