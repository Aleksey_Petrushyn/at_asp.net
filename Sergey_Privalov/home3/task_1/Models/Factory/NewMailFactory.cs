﻿using System;

namespace task_1.Models.Factory
{
    public class NewMailFactory
    {
        private const string defaultMail = "ssdsa.fsd@yandex.by";
        private static readonly string wrongMail = "sda";
        private static readonly string Subject = defaultMail + new Random().Next();
        private static readonly string BodyMail = defaultMail + new Random().Next();

        public static NewMail CorrectMail
        {
            get { return new NewMail(defaultMail, Subject, BodyMail); }
        }
        public static NewMail UncorrectMail
        {
            get { return new NewMail(wrongMail, Subject, BodyMail); }
        }
    }
}
