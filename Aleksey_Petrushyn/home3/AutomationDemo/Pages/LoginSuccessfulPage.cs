﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Framework;
using OpenQA.Selenium;

namespace AutomationDemo.Pages
{
	class LoginSuccessfulPage
	{
		private Browser browser = Browser.Instance;
		private static readonly By writeButtonByCss = By.CssSelector("[class$=-go]");  //search button "Write"
		internal IWebElement WriteButton { get { return browser.FindElement(writeButtonByCss); } }

	}
}
