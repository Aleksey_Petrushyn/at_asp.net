﻿using AutomationTask3.Framework;
using OpenQA.Selenium;

namespace AutomationTask3.Pages
{
    class MailPage
    {
        private Browser browser = Browser.Instance;

        private static readonly By writeMailButtonByCss = By.CssSelector(".ns-view-container-desc a[href='#compose']");
        private static readonly By sendMailButtonByCss = By.CssSelector("button[type='submit']");
        private static readonly By deleteMailButtonByCss = By.CssSelector(".ns-view-toolbar-button-delete");
        private static readonly By checkNewMailButtonByCss = By.CssSelector(".ns-view-toolbar-button-check-mail");
        private static readonly By receivedMailButtonByCss = By.CssSelector(".ns-view-folders a[href='#inbox']");
        private static readonly By removedMailButtonByCss = By.CssSelector(".ns-view-folders a[href='#trash']");
        private static readonly By transmitedMailButtonByCss = By.CssSelector(".ns-view-folders a[href='#sent']");
        //mail fields 
        private static readonly By receiverInputByXpath = By.CssSelector(".mail-Bubbles");
        private static readonly By titleInputByCss = By.CssSelector("input[name='subj']");
        private static readonly By mailTextInputByCss = By.CssSelector("[role='textbox']");

        public IWebElement WriteMailButton { get { return browser.FindElement(writeMailButtonByCss); } }
        public IWebElement SendMailButton { get { return browser.FindElement(sendMailButtonByCss); } }
        public IWebElement ReceiverInput{ get { return browser.FindElement(receiverInputByXpath); } }
        public IWebElement TitleInput { get { return browser.FindElement(titleInputByCss); } }
        public IWebElement MailTextInput { get { return browser.FindElement(mailTextInputByCss); } }
        public IWebElement ReceivedMailButton { get { return browser.FindElement(receivedMailButtonByCss); } }
        public IWebElement CheckNewMailButton { get { return browser.FindElement(checkNewMailButtonByCss); } }
        public IWebElement TransmitedMailButton { get { return browser.FindElement(transmitedMailButtonByCss); } }
        public IWebElement DeleteMailButton { get { return browser.FindElement(deleteMailButtonByCss); } }
        public IWebElement RemovedMailButton { get { return browser.FindElement(removedMailButtonByCss); } }
        public IWebElement FindMail(string nameOfMail)
        {
            return browser.FindElement(By.XPath("//span[@title='" + nameOfMail + "']/ancestor::a//*[contains(@class, 'nb-checkbox-normal-flag')]"));
        }
    }
}
