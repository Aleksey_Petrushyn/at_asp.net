﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Framework;

namespace TestAutomation.Pages
{
	internal static class MailMainPage
	{
		private static readonly By inboxButton = By.CssSelector("div.ns-view-folders a[href='#inbox']");
		private static readonly By sentButton = By.CssSelector("div.ns-view-folders a[href='#sent']");
		private static readonly By trashButton = By.CssSelector("div.ns-view-folders a[href='#trash']");
		private static readonly By composeButton = By.CssSelector("div.ns-view-toolbar a[href='#compose']");
		private static readonly By refreshButton = By.CssSelector("div[data-click-action='mailbox.check']");
		private static readonly By deleteButton = By.CssSelector("div.mail-Toolbar-Item_delete");
		private static readonly By userName = By.CssSelector("div.mail-User-Name");
		private static readonly By newMessageToDiv = By.CssSelector("div[name='to']");
		private static readonly By newMessageSubjectInput = By.CssSelector("input[name='subj']");
		private static readonly By newMessageBodyInput = By.CssSelector("textarea[role='textbox']");
		private static readonly By sendButton = By.CssSelector("button.js-send");
		private static readonly By messageCheckBox = By.CssSelector("label[data-nb='checkbox']");
		private static readonly By invalidEmailDiv = By.CssSelector("div.mail-Compose-Field-Footnote_error");
		private static By GetMessageSelector(string subject) => By.XPath($"//span[@title='{subject}']/ancestor::a");

		internal static IWebElement InboxButton { get { return Browser.Instance.FindElement(inboxButton); } }
		internal static IWebElement SentButton { get { return Browser.Instance.FindElement(sentButton); } }
		internal static IWebElement TrashButton { get { return Browser.Instance.FindElement(trashButton); } }
		internal static IWebElement ComposeButton { get { return Browser.Instance.FindElement(composeButton); } }
		internal static IWebElement RefreshButton { get { return Browser.Instance.FindElement(refreshButton); } }
		internal static IWebElement DeleteButton { get { return Browser.Instance.FindElement(deleteButton); } }
		internal static IWebElement UserName { get { return Browser.Instance.FindElement(userName); } }
		internal static IWebElement NewMessageToDiv { get { return Browser.Instance.FindElement(newMessageToDiv); } }
		internal static IWebElement NewMessageSubjectInput { get { return Browser.Instance.FindElement(newMessageSubjectInput); } }
		internal static IWebElement NewMessageBodyInput { get { return Browser.Instance.FindElement(newMessageBodyInput); } }
		internal static IWebElement SendButton { get { return Browser.Instance.FindElement(sendButton); } }
		internal static IWebElement InvalidEmailDiv { get { return Browser.Instance.FindElement(invalidEmailDiv); } }

		internal static IWebElement GetMessage(string subject)
		{
			return Browser.Instance.FindElement(GetMessageSelector(subject));
		}

		internal static IWebElement GetMarkMessageCheckbox(string subject)
		{
			return GetMessage(subject).FindElement(messageCheckBox);
		}

		internal static bool ContainsMessage(string subject)
		{
			return Browser.Instance.Contains(GetMessageSelector(subject));
		}

		
	}
}
