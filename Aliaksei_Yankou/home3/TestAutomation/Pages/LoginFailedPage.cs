﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Framework;

namespace TestAutomation.Pages
{
	internal static class LoginFailedPage
	{
		private static readonly By errorMessageByCss = By.CssSelector(".error-msg");

		internal static IWebElement ErrorMessage { get { return Browser.Instance.FindElement(errorMessageByCss); } }
	}
}
