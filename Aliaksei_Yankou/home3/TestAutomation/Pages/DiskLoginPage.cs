﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAutomation.Framework;

namespace TestAutomation.Pages
{
	internal static class DiskLoginPage
	{
		private static readonly By loginInputByCss = By.CssSelector("input[name=login]");
		private static readonly By passwordInputByCss = By.CssSelector("input[name=password]");
		private static readonly By errorMessage = By.CssSelector("body > div:last-child > div:last-child div");

		internal static IWebElement LoginInput { get { return Browser.Instance.FindElement(loginInputByCss); } }
		internal static IWebElement PasswordInput { get { return Browser.Instance.FindElement(passwordInputByCss); } }
		internal static IWebElement ErrorMessage { get { return Browser.Instance.FindElement(errorMessage); } }
	}
}
