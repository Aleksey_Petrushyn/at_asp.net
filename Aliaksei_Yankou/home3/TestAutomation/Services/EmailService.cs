﻿using OpenQA.Selenium;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using TestAutomation.Pages;
using TestAutomation.Services.Models;

namespace TestAutomation.Services
{
	public static class EmailService
	{
		private static readonly int checkInboxEverySeconds = 1;
		private static readonly int checkInboxTimeoutSeconds = 15;


		public static void SendEmail(Message message)
		{
			MailMainPage.ComposeButton.Click();
			MailMainPage.NewMessageToDiv.SendKeys(message.To);
			MailMainPage.NewMessageSubjectInput.SendKeys(message.Subject);
			MailMainPage.NewMessageBodyInput.SendKeys(message.Body);
			MailMainPage.SendButton.Click();
			Task.Delay(500).Wait();
		}

		public static void OpenSent()
		{
			MailMainPage.SentButton.Click();
		}

		public static void OpenInbox()
		{
			MailMainPage.InboxButton.Click();
			Task.Delay(500).Wait();
		}

		public static void OpenTrash()
		{
			MailMainPage.TrashButton.Click();
		}

		public static bool ContainsMessage(Message m)
		{
			return MailMainPage.ContainsMessage(m.Subject);
		}
		
		public static bool ConfirmMessageReceived(Message m)
		{
			for (int s = 0; s < checkInboxTimeoutSeconds; s+= checkInboxEverySeconds)
			{
				MailMainPage.RefreshButton.Click();
				Task.Delay(checkInboxEverySeconds * 1000).Wait();
				if (ContainsMessage(m)) return true;
			}
			return false;
		}

		public static void SelectMessage(Message m)
		{
			MailMainPage.GetMarkMessageCheckbox(m.Subject).Click();
		}

		public static string RetrieveInvalidEmailErrorMessage()
		{
			return MailMainPage.InvalidEmailDiv.Text;
		}

		public static void DeleteSelectedMessages()
		{
			var d = MailMainPage.DeleteButton;
			if (MailMainPage.DeleteButton.Enabled)
			{
				MailMainPage.DeleteButton.Click();
			}
			Task.Delay(500).Wait();
		}
	}
}
