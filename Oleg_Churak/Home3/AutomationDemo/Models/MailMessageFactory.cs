﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationDemo.Models
{
    public class MailMessageFactory
    {
        private static readonly string defaultTextMessage =  "SAMPLE" + new Random().Next().ToString();

        public static MailMessage SampleMessageToDefaultAccount
        {
            get { return new MailMessage(UserAccountFactory.DefaultAccount, defaultTextMessage, defaultTextMessage); }
        }
    }
}
