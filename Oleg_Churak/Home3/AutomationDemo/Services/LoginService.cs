﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationDemo.Models;
using AutomationDemo.Pages;

namespace AutomationDemo.Services
{
    public class LoginService
    {
        private MailLoginPage mailLoginPage = new MailLoginPage();

        public void LoginToMailBox(UserAccount account)
        {
            mailLoginPage.LoginInput.SendKeys(account.Login);
            mailLoginPage.PasswordInput.SendKeys(account.Password);
            mailLoginPage.SubmitButton.Click();
        }

        public string RetrieveUserNameLabelOnSuccessLogin()
        {
            return new MainMailStaticPage().userNameLabel.Text;
        }

        public string RetrieveErrorOnFailedLogin()
        {
            return new LoginFailedPage().ErrorMessage.Text;
        }
    }
}
