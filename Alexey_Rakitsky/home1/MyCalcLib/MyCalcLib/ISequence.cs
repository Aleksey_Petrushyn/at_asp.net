﻿using System.Collections.Generic;

namespace MyCalcLib
{
    public interface ISequence
    {
        List<int> NextInts();
    }
}
