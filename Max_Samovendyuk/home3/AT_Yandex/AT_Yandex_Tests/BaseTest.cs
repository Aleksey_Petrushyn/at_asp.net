﻿using AT_Yandex.Framework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Yandex_Tests
{
    public class BaseTest
    {
        private const string baseUrl = "http://www.yandex.by";

        [SetUp]
        public void StartBrowser() => Browser.Instance.Start().OpenAt(baseUrl);


        [TearDown]
        public void CloseBrowser() => Browser.Instance.Close();
    }
}
