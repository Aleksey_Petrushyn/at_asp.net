﻿using AutoTests.Utilities;
using OpenQA.Selenium;

namespace AutoTests.Pages
{
    class TrashMailPage
    {
        public IWebElement TrashButton { get { return Browser.FindElement(By.CssSelector(".ns-view-folders a[href='#trash']")); } }
        public IWebElement FindLetter(string to, string subj)
        {
            return Browser.FindElement(By.XPath(string.Format(
              @"//span[@title='{0}']/ancestor::div[@class='mail-MessageSnippet-Content']//span[@title='{1}']", to, subj)));
        }
        public IWebElement FindCheckBoxLetter(string to, string subj)
        {
            return Browser.FindElement(By.XPath(string.Format(
              @"//span[@title='{0}']/ancestor::div[@class='mail-MessageSnippet-Content']//span[@title='{1}']
                /ancestor::div[@class='mail-MessageSnippet-Content']//label[@data-nb='checkbox']", to, subj)));
        }
        public IWebElement DeleteButton { get { return Browser.FindElement(By.CssSelector("div.ns-view-toolbar-button-delete")); } }
    }
}

