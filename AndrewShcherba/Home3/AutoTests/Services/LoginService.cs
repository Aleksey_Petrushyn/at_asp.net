﻿using AutoTests.Pages;
using AutoTests.Models;

namespace AutoTests.Services
{
    public class LoginService
    {
        private LoginPage mailLoginPage = new LoginPage();

        public void LoginToMailBox(UserAccount account)
        {
            mailLoginPage.LoginInput.SendKeys(account.Login);
            mailLoginPage.PasswordInput.SendKeys(account.Password);
            mailLoginPage.SubmitButton.Click();
        }

        public string RetrieveErrorOnFailedLogin()
        {
            return new LoginFailedPage().ErrorMessage.Text;
        }

        public string ChekMailAcount()
        {
            return new MailMainPage().LoginMail.Text;
        }
    }
}
