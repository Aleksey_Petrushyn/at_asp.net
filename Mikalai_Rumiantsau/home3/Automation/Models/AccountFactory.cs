﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Models
{
    class AccountFactory
    {
        private const string defaultMailUserLogin = "nikolaiby1984";
        private const string defaultMailUserPassword = "hevzywtd";
        private const string defaultUserEmail = defaultMailUserLogin + "@yandex.ru";

        private static readonly string WRONG_PASSWORD = defaultMailUserPassword + new Random().Next();
        private static readonly string NONEXISTENT_LOGIN = defaultMailUserLogin + new Random().Next();

         public static UserAccount Account
        {
            get { return new UserAccount(defaultMailUserLogin, defaultMailUserPassword, defaultUserEmail); }
        }

        public static UserAccount AccountWithWrongPassword
        {
            get { return new UserAccount(defaultMailUserLogin, WRONG_PASSWORD, defaultUserEmail); }
        }

        public static UserAccount NonExistedAccount
        {
            get { return new UserAccount(NONEXISTENT_LOGIN, defaultMailUserPassword, defaultUserEmail); }
        }
    }
}
