﻿using Automation.Framework;
using Automation.Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Pages
{
    class MailSendPage
    {
        private Browser browser = Browser.Instance;
        private static readonly By deleteButtonByXPath = By.XPath("//div[contains(@title, 'Удалить (Delete)')]");
        public IWebElement findTestLetter(By str) {  return browser.FindElement(str); } 
        public IWebElement deleteButton { get { return browser.FindElement(deleteButtonByXPath); } }
    }
}
