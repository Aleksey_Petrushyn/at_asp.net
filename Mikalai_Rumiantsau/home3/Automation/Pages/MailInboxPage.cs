﻿using Automation.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Pages
{
    class MailInboxPage
    {
        private Browser browser = Browser.Instance;
        private static readonly By findTestLetterByXPath = By.XPath("//span[@title='test']/ancestor::a");
        public IWebElement findTestLetter(By str) { return browser.FindElement(str); }
    }
}
